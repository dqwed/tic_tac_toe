from tictactoe import check_conditions
import pytest


win_conditions = ((1, 2, 3), (4, 5, 6), (7, 8, 9), (1, 4, 7),
                  (2, 5, 8), (3, 6, 9), (1, 5, 9), (3, 5, 7))


def test_check_conditions_func():
    assert check_conditions(['x', 2, 3, 'x', 5, 6, 'x', 8, 9]) == 'x'


def test_check_conditions_func_2():
    assert check_conditions(['o', 2, 3, 4, 'o', 6, 7, 8, 'o']) == 'o'


def test_check_conditions_func_3():
    with pytest.raises(IndexError):
        assert check_conditions(['x', 'x'])


def test_check_conditions_func_4():
    with pytest.raises(TypeError):
        assert check_conditions(0)
