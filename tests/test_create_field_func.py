from tictactoe import create_field


def test_create_field():
    assert create_field() == [1, 2, 3, 4, 5, 6, 7, 8, 9]


def test_create_field_2():
    assert create_field() != []
