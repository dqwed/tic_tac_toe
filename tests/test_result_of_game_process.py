from typing import List, Union
import pytest


def move(sign: str, field: List[Union[int, str]], position: int) -> bool:
    if 1 <= position <= 9:
        if isinstance(field[position - 1], int):
            field[position - 1] = sign
            return True
    return False


def test_move():
    field = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert move('x', field, 3) is True


def test_move_1():
    field = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    with pytest.raises(AssertionError):
        assert move('o', field, 10)


def test_move_w():
    field = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    with pytest.raises(TypeError):
        assert move('o', field, 's')