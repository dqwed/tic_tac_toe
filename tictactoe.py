from typing import Union, List, Callable


# процесс игры
def main() -> None:
    field = create_field()
    print(f'Чтобы походить введите координату - число, '
          f'куда хотите поставить свой знак.')
    wrapped = draw_beautiful_field(draw_field, field)
    winner = result_of_game_process(field)
    print(f'Победили {winner}.' if winner else 'Ничья.')
    print(f'Конец игры.')


# вычислительная часть игры
def result_of_game_process(field: List[Union[int, str]]) -> Union[str, bool]:
    winner = False
    for i in range(9):
        winner = check_conditions(field)
        if not winner:
            move('x' if i % 2 == 0 else 'o', field)
        draw_beautiful_field(draw_field, field)
    return winner


# функция для того чтобы походить
def move(sign: str, field: List[Union[int, str]]) -> None:
    print('Ходит', sign + '.')
    correct = False
    while not correct:
        print(f'Введите номер позиции, на которую хотите '
              f'поставить {sign}.')
        position = input()
        try:
            position = int(position)
        except ValueError:
            print(f'Некорректный формат ввода. Попробуйте ещё раз.')
            continue
        if 1 <= position <= 9:
            if type(field[position - 1]) == int:
                field[position - 1] = sign
                correct = True
            else:
                print('Эта клетка занята.')
        else:
            print(f'Неверное число. Число может быть >= 1 и <= 9.')


# декоратор чтобы избавиться от мусора
def draw_beautiful_field(func: Callable, field: List[Union[int, str]]):
    def wrapper(field):
        print('\nПоле:')
        func(field)
        print()
    return wrapper(field)


# отрисовка поля
def draw_field(field: List[Union[int, str]]) -> None:
    for i in range(3):
        print(*field[i * 3:i * 3 + 3])


# создание поля
def create_field() -> List[Union[int, str]]:
    return [
        1, 2, 3,
        4, 5, 6,
        7, 8, 9
    ]


# проверка на победу
def check_conditions(field: List[Union[int, str]]) -> \
        Union[bool, Union[int, str]]:
    win_conditions = ((1, 2, 3), (4, 5, 6), (7, 8, 9), (1, 4, 7),
                      (2, 5, 8), (3, 6, 9), (1, 5, 9), (3, 5, 7))
    for condition in win_conditions:
        if (field[condition[0] - 1] == field[condition[1] - 1] ==
                field[condition[2] - 1]):
            return field[condition[0] - 1]
    return False


if __name__ == '__main__':
    main()
